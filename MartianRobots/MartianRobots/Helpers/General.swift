//
//  General.swift
//  MartianRobots
//
//  Created by paul calver on 16/01/2019.
//  Copyright © 2019 paul calver. All rights reserved.
//

import Foundation
import UIKit

public struct Constants {
    static let debugLog = false
}

enum Orientation: String {
    case North = "N"
    case South = "S"
    case East = "E"
    case West = "W"
}

enum Direction: String {
    case Left = "L"
    case Right = "R"
    case Forward = "F"
}

struct PointAndOrientation: Equatable, CustomDebugStringConvertible {
    let point: CGPoint
    let orientation: Orientation
    
    var debugDescription: String {
        return "point: \(point) orientation: \(orientation)"
    }
    
    static func == (lhs: PointAndOrientation, rhs: PointAndOrientation) -> Bool {
        return (lhs.point == rhs.point && lhs.orientation == rhs.orientation)
    }
}
