//
//  Helpers.swift
//  MartianRobots
//
//  Created by paul calver on 16/01/2019.
//  Copyright © 2019 paul calver. All rights reserved.
//

import Foundation
import UIKit

// MARK: helper functions
func convertToPoint(_ string: String?) -> CGPoint? {
    
    guard let string = string, !string.isEmpty else { return nil }
    
    let coords = string.components(separatedBy: CharacterSet.whitespaces)
    
    guard let x: Int = Int(coords[0]), let y: Int = Int(coords[1]) else { return nil }
    
    return CGPoint(x: x, y: y)
}

func extractRobotPositionAndOrientation(_ string: String?) -> PointAndOrientation? {
    
    guard let string = string, !string.isEmpty , let point = convertToPoint(string), let orientationData = string.last, let orientation = Orientation(rawValue: String(orientationData)) else { return nil }
    
    return PointAndOrientation(point: point, orientation: orientation)
}

func gridOffsetsAndOrientationFor(direction: Direction, withOrientation orientation: Orientation) -> PointAndOrientation {
    
    var xOffset: CGFloat = 0
    var yOffset: CGFloat = 0
    var newOrientation: Orientation = orientation
    
    switch direction {
    case .Left:
        switch orientation {
        case .North:
            newOrientation = .West
        case .South:
            newOrientation = .East
        case .East:
            newOrientation = .North
        case .West:
            newOrientation = .South
        }
    case .Right:
        switch orientation {
        case .North:
            newOrientation = .East
        case .South:
            newOrientation = .West
        case .East:
            newOrientation = .South
        case .West:
            newOrientation = .North
        }
    case .Forward:
        switch orientation {
        case .North:
            yOffset = 1
        case .South:
            yOffset = -1
        case .East:
            xOffset = 1
        case .West:
            xOffset = -1
        }
    }
    
    let point = CGPoint(x: xOffset, y: yOffset)
    let ret = PointAndOrientation(point: point, orientation: newOrientation)
    return ret
}
