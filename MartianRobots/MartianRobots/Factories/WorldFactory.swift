//
//  WorldFactory.swift
//  MartianRobots
//
//  Created by paul calver on 16/01/2019.
//  Copyright © 2019 paul calver. All rights reserved.
//

import Foundation
import UIKit

struct WorldFactory {

    static func create(fromInput input: Input?) -> World? {
        
        guard let input = input, let grid = convertToPoint(input.worldDetail.grid) else { return nil }
        
        let worldFrame = CGRect(x: 0, y: 0, width: grid.x, height: grid.y)
        let robots = input.robotDetails.compactMap { RobotFactory.createRobotFrom(robotDetail: $0) }
        
        return World(frame: worldFrame, robots: robots)
    }
}
