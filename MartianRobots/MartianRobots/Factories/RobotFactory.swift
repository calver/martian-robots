//
//  RobotFactory.swift
//  MartianRobots
//
//  Created by paul calver on 16/01/2019.
//  Copyright © 2019 paul calver. All rights reserved.
//

import Foundation

struct RobotFactory {
    
    static func createRobotFrom(robotDetail detail: RobotInputDetail?) -> Robot? {
        guard let detail = detail, let positionAndOrientation = extractRobotPositionAndOrientation(detail.position) else { return nil }
        
        return Robot(orientation: positionAndOrientation.orientation, position: positionAndOrientation.point, commands: detail.commands)
    }
}
