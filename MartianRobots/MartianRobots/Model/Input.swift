//
//  InputData.swift
//  MartianRobots
//
//  Created by paul calver on 16/01/2019.
//  Copyright © 2019 paul calver. All rights reserved.
//

import Foundation

protocol InputType {
    func worldGrid() -> String
    func numberOfRobots() -> Int
}

struct Input: InputType {
    let worldDetail: WorldInputDetail
    let robotDetails: [RobotInputDetail]
    
    func worldGrid() -> String {
        return worldDetail.grid
    }
    
    func numberOfRobots() -> Int {
        return robotDetails.count
    }
}

struct WorldInputDetail {
    let grid: String
}

struct RobotInputDetail {
    let position: String
    let commands: RobotCommands
}
