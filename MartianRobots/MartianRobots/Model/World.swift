//
//  World.swift
//  MartianRobots
//
//  Created by paul calver on 16/01/2019.
//  Copyright © 2019 paul calver. All rights reserved.
//

import Foundation
import UIKit

protocol WorldType {
    var worldFrame: CGRect { get }
    var robotsCount: Int { get }
    var worldRobots: [Robot] { get }
    
    func runRobots()
    func robotsReport() -> [String]
}

class World: WorldType, CustomDebugStringConvertible {
    
    fileprivate let frame: CGRect
    fileprivate let robots: [Robot]
    fileprivate var doomedPositions: [PointAndOrientation]
    
    public var worldFrame: CGRect {
        return frame
    }
    
    public var robotsCount: Int {
        return robots.count
    }
    
    public var worldRobots: [Robot] {
       return robots
    }

    init(frame: CGRect, robots: [Robot]) {
        self.frame = frame
        self.robots = robots
        self.doomedPositions = [PointAndOrientation]()
    }
    
    var debugDescription: String {
        return "frame: \(frame) robots: \(robots) doomedPositions: \(doomedPositions)"
    }
    
    public func runRobots() {
        _ = robots.map { runCommandsForRobot($0) }
    }
    
    public func robotsReport() -> [String] {
        let report = robots.map { $0.printScent() }
        return report
    }
    
    fileprivate func runCommandsForRobot(_ robot: Robot) {
        
        robot.processCommandsWithinWorld(self, withKnownDoomedPositions: doomedPositions)
        
        if robot.isRobotLost {
            let doomedPointAndOrientation = PointAndOrientation(point: robot.currentPosition, orientation: robot.currentOrientation)
            doomedPositions.append(doomedPointAndOrientation)
        }
    }
}
