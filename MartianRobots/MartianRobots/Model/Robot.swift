//
//  Robot.swift
//  MartianRobots
//
//  Created by paul calver on 16/01/2019.
//  Copyright © 2019 paul calver. All rights reserved.
//

import Foundation
import UIKit

typealias RobotCommands = String

protocol RobotType {
    var isRobotLost: Bool { get }
    var currentPosition: CGPoint { get }
    var currentOrientation: Orientation { get }
    var robotCommands: RobotCommands { get }
    
    func perfomCommandStep(_ step: String, world: World, knownDoomedPositions doomedPositions: [PointAndOrientation])
}

class Robot: RobotType, CustomDebugStringConvertible, Equatable {
    
    fileprivate var orientation: Orientation
    fileprivate var position: CGPoint
    fileprivate let commands: RobotCommands
    fileprivate var scent: String
    fileprivate var isLost: Bool
    
    var isRobotLost: Bool {
        return isLost
    }
    
    var currentPosition: CGPoint {
        return position
    }
    
    var currentOrientation: Orientation {
        return orientation
    }
    
    var robotCommands: RobotCommands {
        return commands
    }
    
    init(orientation: Orientation, position: CGPoint, commands: RobotCommands) {
        self.orientation = orientation
        self.position = position
        self.commands = commands
        self.scent = ""
        self.isLost = false
    }
    
    var debugDescription: String {
        return "position: \(position) orientation: \(orientation) isLost: \(isLost), scent: \(scent)"
    }
    
    public func processCommandsWithinWorld(_ world: World, withKnownDoomedPositions doomedPositions: [PointAndOrientation]) {
        
        if Constants.debugLog {
            print("processCommandsWithWorldFrame frame: \(world.worldFrame) doomedPositions: \(doomedPositions)")
        }
        
        guard !commands.isEmpty else { return }
        
        _ = commands.map { String($0) }.map { perfomCommandStep($0, world: world, knownDoomedPositions:  doomedPositions) }
    }
    
    public func perfomCommandStep(_ step: String, world: World, knownDoomedPositions doomedPositions: [PointAndOrientation]) {
        
        let startingGridPosition = position
        let startingOrientation = orientation
        
        if Constants.debugLog {
            print("perfomCommandStep step: \(step), world: \(world),startingGridPosition: \(startingGridPosition), startingOrientation: \(startingOrientation), knownDoomedPositions: \(doomedPositions)")
        }
        
        guard isLost == false, let direction: Direction = Direction(rawValue: step) else { return }
        
        if direction == .Forward {
            let currentPointAndOrientation = PointAndOrientation(point: startingGridPosition, orientation: startingOrientation)
            if doomedPositions.contains(currentPointAndOrientation) { return }
        }
        
        let gridOffsetsAndOrientation = gridOffsetsAndOrientationFor(direction: direction, withOrientation: startingOrientation)
        
        let newPosition = startingGridPosition + gridOffsetsAndOrientation.point
        let newOrientation = gridOffsetsAndOrientation.orientation
        
        var updatedScent = makeScent(fromPosition: newPosition, andOrientation: newOrientation)
        
        if hasFallenOffWorld(newPosition, worldFrame: world.worldFrame) {
            isLost = true
            updatedScent = makeScent(fromPosition: startingGridPosition, andOrientation: startingOrientation)
            updatedScent += " LOST"
        }
        
        scent = updatedScent
        
        if Constants.debugLog {
            print("robot starting at: \(startingGridPosition) orientation: \(orientation)")
            print("command: \(direction), new Position : \(newPosition) new Orientation \(newOrientation)")
            print("scent: \(updatedScent)")
            print("isLost: \(isLost)")
        }
        
        if isLost { return }
        
        position = newPosition
        orientation = newOrientation
    }
    
    public func printScent() -> String {
        return scent
    }
    
    fileprivate func hasFallenOffWorld(_ point: CGPoint, worldFrame frame: CGRect) -> Bool {
        return (point.x < 0 || point.x > frame.maxX || point.y < 0 || point.y > frame.maxY)
    }
    
    fileprivate func makeScent(fromPosition position: CGPoint, andOrientation orientation: Orientation) -> String {
        return "\(Int(position.x)) \(Int(position.y)) \(orientation.rawValue)"
    }
    
    static func == (lhs: Robot, rhs: Robot) -> Bool {
        return (lhs.orientation == rhs.orientation && lhs.position == rhs.position && lhs.commands == rhs.commands && lhs.scent == rhs.scent && lhs.isLost == rhs.isLost)
    }
}
