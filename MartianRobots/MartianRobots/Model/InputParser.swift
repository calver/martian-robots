//
//  InputParser.swift
//  MartianRobots
//
//  Created by paul calver on 16/01/2019.
//  Copyright © 2019 paul calver. All rights reserved.
//

import Foundation

struct InputParser {
    
    static func parse(_ input: String) -> Input? {
        
        guard !input.isEmpty else { return nil }
        
        let lines = input.components(separatedBy: CharacterSet.newlines).filter{ !$0.isEmpty }
        guard let worldGrid = lines.first else { return nil }
        
        let worldDetail = WorldInputDetail(grid: worldGrid)
        
        let remaining = Array(lines.dropFirst(1))
        
        guard remaining.count >= 2 else { return nil }
        
        let robotData = remaining.chunked(into: 2)
        
        var robotDetails = [RobotInputDetail]()
        
        for data in robotData {
            guard let position = data.first, let commands = data.last else { continue }
            let detail = RobotInputDetail(position: position, commands: commands)
            robotDetails.append(detail)
        }
        
        return Input(worldDetail: worldDetail, robotDetails: robotDetails)
    }
}
