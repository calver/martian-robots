//
//  RobotTests.swift
//  MartianRobotsTests
//
//  Created by paul calver on 16/01/2019.
//  Copyright © 2019 paul calver. All rights reserved.
//

import XCTest
@testable import MartianRobots

class RobotTests: XCTestCase {

    let inputRobotIsNotDoomed = """
5 3
1 1 E
RFRFRFRF
"""
    let inputRobotIsDoomed = """
2 2
2 2 E
RFRFRFRF
"""
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func test_Robot_NotLost_CommandChangesOrientation_RobotHasExpectedPositionAndOrientation() {
        
        let input = InputParser.parse(inputRobotIsNotDoomed)
        XCTAssertNotNil(input)
        
        let world = WorldFactory.create(fromInput: input)
        XCTAssertNotNil(world)
        
        let robot = world?.worldRobots.first
        XCTAssertNotNil(robot)
        
        robot?.perfomCommandStep("L", world: world!, knownDoomedPositions: [])
        
        let expectedPosition = CGPoint(x: 1, y: 1)
        let expectedOrientation: Orientation = .North
        
        XCTAssertEqual(robot!.currentPosition, expectedPosition)
        XCTAssertEqual(robot!.currentOrientation, expectedOrientation)
    }
    
    func test_Robot_NotLost_CommandChangesPosition_RobotHasExpectedPositionAndOrientation() {
        
        let input = InputParser.parse(inputRobotIsNotDoomed)
        XCTAssertNotNil(input)
        
        let world = WorldFactory.create(fromInput: input)
        XCTAssertNotNil(world)
        
        let robot = world?.worldRobots.first
        XCTAssertNotNil(robot)
        
        robot?.perfomCommandStep("F", world: world!, knownDoomedPositions: [])
        
        let expectedPosition = CGPoint(x: 2, y: 1)
        let expectedOrientation: Orientation = .East
        
        XCTAssertEqual(robot!.currentPosition, expectedPosition)
        XCTAssertEqual(robot!.currentOrientation, expectedOrientation)
    }
    
    func test_Robot_NotLost_CommandWouldMakeRobotDropOffWorld_RobotHasExpectedPositionOrientationScentAndIsLost() {
        
        let input = InputParser.parse(inputRobotIsDoomed)
        XCTAssertNotNil(input)
        
        let world = WorldFactory.create(fromInput: input)
        XCTAssertNotNil(world)
        
        let robot = world?.worldRobots.first
        XCTAssertNotNil(robot)
        
        robot?.perfomCommandStep("F", world: world!, knownDoomedPositions: [])
        
        let expectedPosition = CGPoint(x: 2, y: 2)
        let expectedOrientation: Orientation = .East
        let expectedScent = "2 2 E LOST"
        let expectedIsLost = true
        
        XCTAssertEqual(robot!.currentPosition, expectedPosition)
        XCTAssertEqual(robot!.currentOrientation, expectedOrientation)
        XCTAssertEqual(robot!.printScent(), expectedScent)
        XCTAssertEqual(robot!.isRobotLost, expectedIsLost)
    }
    
    func test_Robot_NotLost_CommandWouldDoomRobot_CommandShouldBeIgnored() {
        
        let input = InputParser.parse(inputRobotIsDoomed)
        XCTAssertNotNil(input)
        
        let world = WorldFactory.create(fromInput: input)
        XCTAssertNotNil(world)
        
        let robot = world?.worldRobots.first
        XCTAssertNotNil(robot)
        
        let doomedPosition = PointAndOrientation(point: CGPoint(x: 2, y: 2), orientation: .East)
        
        robot?.perfomCommandStep("F", world: world!, knownDoomedPositions: [doomedPosition])
        
        let expectedPosition = CGPoint(x: 2, y: 2)
        let expectedOrientation: Orientation = .East
        let expectedScent = ""
        let expectedIsLost = false
        
        XCTAssertEqual(robot!.currentPosition, expectedPosition)
        XCTAssertEqual(robot!.currentOrientation, expectedOrientation)
        XCTAssertEqual(robot!.printScent(), expectedScent)
        XCTAssertEqual(robot!.isRobotLost, expectedIsLost)
    }
}
