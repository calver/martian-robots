//
//  RobotFactoryTests.swift
//  MartianRobotsTests
//
//  Created by paul calver on 16/01/2019.
//  Copyright © 2019 paul calver. All rights reserved.
//

import XCTest
@testable import MartianRobots

class RobotFactoryTests: XCTestCase {

    let emptyInput = ""
    let invalidInputOnlyGrid = """
    5 3
    """
    let fullValidInput = """
5 3
1 1 E
RFRFRFRF

3 2 N
FRRFLLFFRRFLL

0 3 W
LLFFFLFLFL
"""
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func test_EmptyInput_Factory_CreatesNilRobots() {
        
        let input = InputParser.parse(emptyInput)
        let robots = input?.robotDetails.compactMap { RobotFactory.createRobotFrom(robotDetail: $0) }
        
        XCTAssertNil(robots)
    }
    
    func test_InvalidInput_Factory_CreatesNilWorld() {
        
        let input = InputParser.parse(invalidInputOnlyGrid)
        let robots = input?.robotDetails.compactMap { RobotFactory.createRobotFrom(robotDetail: $0) }
        
        XCTAssertNil(robots)
    }
    
    func test_ValidInput_Factory_CreatesWorldWithExpectedNumberOfRobots() {
        
        let input = InputParser.parse(fullValidInput)
        XCTAssertNotNil(input)
        
        let robots = input?.robotDetails.compactMap { RobotFactory.createRobotFrom(robotDetail: $0) }
        
        let expected = 3
        
        XCTAssertEqual(robots?.count, expected)
    }
    
    func test_GivenRobotDetail_Factory_CreatesRobotWithExpectedProperties() {
        
        let positionInput = "1 1 E"
        let commandsInput = "RFRFRFRF"
        
        let robotDetail = RobotInputDetail(position: positionInput, commands: commandsInput)
        
        let expectedPosition = CGPoint(x: 1, y: 1)
        let expectedOrientation: Orientation = .East
        let expectedCommands = commandsInput
        
        let factoryRobot = RobotFactory.createRobotFrom(robotDetail: robotDetail)
        
        XCTAssertNotNil(factoryRobot)
        
        let expectedRobot = Robot(orientation: expectedOrientation, position: expectedPosition, commands: expectedCommands)
        
        XCTAssertEqual(factoryRobot!, expectedRobot)
    }
}
