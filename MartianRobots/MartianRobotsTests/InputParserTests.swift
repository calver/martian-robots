//
//  InputParserTests.swift
//  MartianRobotsTests
//
//  Created by paul calver on 16/01/2019.
//  Copyright © 2019 paul calver. All rights reserved.
//

import XCTest
@testable import MartianRobots

class InputParserTests: XCTestCase {

    let emptyInput = ""
    let invalidInputOnlyGrid = """
    5 3
    """
    let invalidInputMissingRobotDetail = """
5 3
1 1 E
"""
    
    let fullValidInput = """
5 3
1 1 E
RFRFRFRF

3 2 N
FRRFLLFFRRFLL

0 3 W
LLFFFLFLFL
"""
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func test_EmptyInput_HaveNilInputType() {
        let input = InputParser.parse(emptyInput)
        XCTAssertNil(input)
    }
    
    func test_ValidInput_GridHasExpectedValue() {
        
        let input = InputParser.parse(fullValidInput)
        let expected = "5 3"
        
        XCTAssertNotNil(input)
        XCTAssertEqual(input?.worldGrid(), expected)
    }
    
    func test_ValidInput_InputTypeHasExpectedNumberOfRobots() {

        let input = InputParser.parse(fullValidInput)
        let expected = 3
        
        XCTAssertNotNil(input)
        XCTAssertEqual(input?.numberOfRobots(), expected)
    }
}
