//
//  WorldFactoryTests.swift
//  MartianRobotsTests
//
//  Created by paul calver on 16/01/2019.
//  Copyright © 2019 paul calver. All rights reserved.
//

import XCTest
@testable import MartianRobots

class WorldFactoryTests: XCTestCase {

    let emptyInput = ""
    let invalidInputOnlyGrid = """
    5 3
    """
    let fullValidInput = """
5 3
1 1 E
RFRFRFRF

3 2 N
FRRFLLFFRRFLL

0 3 W
LLFFFLFLFL
"""
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func test_EmptyInput_Factory_CreatesNilWorld() {
        
        let input = InputParser.parse(emptyInput)
        let world = WorldFactory.create(fromInput: input)
        XCTAssertNil(world)
    }
    
    func test_InvalidInput_Factory_CreatesNilWorld() {
        
        let input = InputParser.parse(invalidInputOnlyGrid)
        let world = WorldFactory.create(fromInput: input)
        XCTAssertNil(world)
    }
    
    func test_ValidInput_Factory_CreatesWorldWithExpectedFrame() {
        
        let input = InputParser.parse(fullValidInput)
        XCTAssertNotNil(input)
        
        let world = WorldFactory.create(fromInput: input)
        
        let expected = CGRect(x: 0, y: 0, width: 5, height: 3)
        
        XCTAssertEqual(world?.worldFrame, expected)
    }
    
    func test_ValidInput_Factory_CreatesWorldWithExpectedNumberOfRobots() {
        
        let input = InputParser.parse(fullValidInput)
        XCTAssertNotNil(input)
        
        let world = WorldFactory.create(fromInput: input)
        
        let expected = 3
        
        XCTAssertEqual(world?.robotsCount, expected)
    }
}
