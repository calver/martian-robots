import PlaygroundSupport
import UIKit

typealias aClosure = () -> ()
typealias RobotCommands = String

extension Array {
    func chunked(into size: Int) -> [[Element]] {
        return stride(from: 0, to: count, by: size).map {
            Array(self[$0 ..< Swift.min($0 + size, count)])
        }
    }
}

extension CGPoint {
    static func + (lhs: CGPoint, rhs: CGPoint) -> CGPoint {
        return CGPoint(x: lhs.x + rhs.x, y: lhs.y + rhs.y)
    }
}

enum Orientation: String {
    case North = "N"
    case South = "S"
    case East = "E"
    case West = "W"
}

enum Direction: String {
    case Left = "L"
    case Right = "R"
    case Forward = "F"
}

// MARK: input data

struct Input {
    let worldDetail: WorldInputDetail
    let robotDetails: [RobotInputDetail]
}

struct WorldInputDetail {
    let grid: String
}

struct RobotInputDetail {
    let position: String
    let commands: RobotCommands
}

// MARK: 
struct PointAndOrientation: Equatable, CustomDebugStringConvertible {
    let point: CGPoint
    let orientation: Orientation
    
    var debugDescription: String {
        return "point: \(point) orientation: \(orientation)"
    }
    
    static func == (lhs: PointAndOrientation, rhs: PointAndOrientation) -> Bool {
        return (lhs.point == rhs.point && lhs.orientation == rhs.orientation)
    }
}

// MARK:

class Robot {
    fileprivate var orientation: Orientation
    fileprivate var position: CGPoint
    fileprivate let commands: RobotCommands
    fileprivate var scent: String
    fileprivate var isLost: Bool
    
    init(orientation: Orientation, position: CGPoint, commands: RobotCommands) {
        self.orientation = orientation
        self.position = position
        self.commands = commands
        self.scent = ""
        self.isLost = false
    }

    public func processCommandsWithinWorld(_ world: World, withKnownDoomedPositions doomedPositions: [PointAndOrientation]) {
        
        if debug {
            print("processCommandsWithWorldFrame frame: \(world.worldFrame) doomedPositions: \(doomedPositions)")
        }
        
        guard !commands.isEmpty else { return }
    
        let steps = commands.map { String($0) }
        
        for step in steps {
            
            let startingGridPosition = position
            let startingOrientation = orientation
            
            guard let direction: Direction = Direction(rawValue: step) else { continue }
            
            if direction == .Forward {
                let currentPointAndOrientation = PointAndOrientation(point: startingGridPosition, orientation: startingOrientation)
                if doomedPositions.contains(currentPointAndOrientation) { continue }
            }
    
            let gridOffsetsAndOrientation = gridOffsetsAndOrientationFor(direction: direction, withOrientation: startingOrientation)
            
            let newPosition = startingGridPosition + gridOffsetsAndOrientation.point
            let newOrientation = gridOffsetsAndOrientation.orientation
            
            var updatedScent = makeScent(fromPosition: newPosition, andOrientation: newOrientation)
        
            if hasFallenOffWorld(newPosition, worldFrame: world.worldFrame) {
                isLost = true
                updatedScent = makeScent(fromPosition: startingGridPosition, andOrientation: startingOrientation)
                updatedScent += " LOST"
            }
            
            scent = updatedScent
            
            if debug {
                print("robot starting at: \(startingGridPosition) orientation: \(orientation)")
                print("command: \(direction), new Position : \(newPosition) new Orientation \(newOrientation)")
                print("scent: \(updatedScent)")
                print("isLost: \(isLost)")
            }
        
            if isLost {
                break
            }
            
            position = newPosition
            orientation = newOrientation
        }
    }
    
    fileprivate func hasFallenOffWorld(_ point: CGPoint, worldFrame frame: CGRect) -> Bool {
        return (point.x < 0 || point.x > frame.maxX || point.y < 0 || point.y > frame.maxY)
    }
    
    fileprivate func makeScent(fromPosition position: CGPoint, andOrientation orientation: Orientation) -> String {
        return "\(Int(position.x)) \(Int(position.y)) \(orientation.rawValue)"
    }
}

class World {
    fileprivate let frame: CGRect
    fileprivate let robots: [Robot]
    fileprivate var doomedPositions: [PointAndOrientation]
    
    public var worldFrame: CGRect {
        return frame
    }
    
    init(frame: CGRect, robots: [Robot]) {
        self.frame = frame
        self.robots = robots
        self.doomedPositions = [PointAndOrientation]()
    }
    
    public func runRobots(_ completion: aClosure) {
        robots.map { runCommandsForRobot($0) }
        completion()
    }
    
    public func issueRobotReport() {
        robots.map { print($0.scent ) }
    }
    
    fileprivate func runCommandsForRobot(_ robot: Robot) {
        
        robot.processCommandsWithinWorld(self, withKnownDoomedPositions: doomedPositions)
        
        if robot.isLost {
            let doomedPointAndOrientation = PointAndOrientation(point: robot.position, orientation: robot.orientation)
            doomedPositions.append(doomedPointAndOrientation)
        }
    }
}

struct InputParser {
    
    public func parse(_ input: String) -> Input? {
        
        guard !input.isEmpty else { return nil }
        
        let lines = input.components(separatedBy: CharacterSet.newlines).filter{ !$0.isEmpty }
        guard let worldGrid = lines.first else { return nil }
        
        let worldDetail = WorldInputDetail(grid: worldGrid)
        
        let remaining = Array(lines.dropFirst(1))
        let robotData = remaining.chunked(into: 2)
        
        var robotDetails = [RobotInputDetail]()
    
        for data in robotData {
            guard let position = data.first, let commands = data.last else { continue }
            let detail = RobotInputDetail(position: position, commands: commands)
            robotDetails.append(detail)
        }
        
        return Input(worldDetail: worldDetail, robotDetails: robotDetails)
    }
}

// MARK: factorys

struct RobotFactory {
    static func createRobotFrom(robotDetail detail: RobotInputDetail) -> Robot? {
        guard let positionAndOrientation = extractRobotPositionAndOrientation(detail.position) else { return nil }
        
        return Robot(orientation: positionAndOrientation.orientation, position: positionAndOrientation.point, commands: detail.commands)
    }
}

struct WorldFactory {
    
    public func create(fromInput input: Input) -> World? {
        
        guard let grid = convertToPoint(input.worldDetail.grid) else { return nil }
        
        let worldFrame = CGRect(x: 0, y: 0, width: grid.x, height: grid.y)
        let robots = input.robotDetails.compactMap { RobotFactory.createRobotFrom(robotDetail: $0) }
        
        return World(frame: worldFrame, robots: robots)
    }
}

// MARK: helper functions

func convertToPoint(_ string: String?) -> CGPoint? {
    
    guard let string = string, !string.isEmpty else { return nil }
    
    let coords = string.components(separatedBy: CharacterSet.whitespaces)
    
    guard let x: Int = Int(coords[0]), let y: Int = Int(coords[1]) else { return nil }
    
    return CGPoint(x: x, y: y)
}

func extractRobotPositionAndOrientation(_ string: String?) -> PointAndOrientation? {
    
    guard let string = string, !string.isEmpty , let point = convertToPoint(string), let orientationData = string.last, let orientation = Orientation(rawValue: String(orientationData)) else { return nil }
    
    return PointAndOrientation(point: point, orientation: orientation)
}

func gridOffsetsAndOrientationFor(direction: Direction, withOrientation orientation: Orientation) -> PointAndOrientation {
    
    var xOffset: CGFloat = 0
    var yOffset: CGFloat = 0
    var newOrientation: Orientation = orientation
    
    switch direction {
    case .Left:
        switch orientation {
        case .North:
            newOrientation = .West
        case .South:
            newOrientation = .East
        case .East:
            newOrientation = .North
        case .West:
            newOrientation = .South
        }
    case .Right:
        switch orientation {
        case .North:
            newOrientation = .East
        case .South:
            newOrientation = .West
        case .East:
            newOrientation = .South
        case .West:
            newOrientation = .North
        }
    case .Forward:
        switch orientation {
        case .North:
            yOffset = 1
        case .South:
            yOffset = -1
        case .East:
            xOffset = 1
        case .West:
            xOffset = -1
        }
    }
    
    let point = CGPoint(x: xOffset, y: yOffset)
    let ret = PointAndOrientation(point: point, orientation: newOrientation)
    return ret
}

// MARK:

let debug = false

let initialInput = """
5 3
1 1 E
RFRFRFRF

3 2 N
FRRFLLFFRRFLL

0 3 W
LLFFFLFLFL
"""

let worldFactory = WorldFactory()
let parser = InputParser()

if let input = parser.parse(initialInput), let world = worldFactory.create(fromInput: input) {
    world.runRobots {
        world.issueRobotReport()
    }
}

